
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import api.methods.ColorUtil;
import api.methods.Mouse;
import api.methods.Tabs;
import bot.script.Script;
import bot.script.ScriptManifest;

@ScriptManifest(authors = { "Active" }, category = "Magic", name = "Alcher", description = "Basic Alcher")
public class ActiveAlcher extends Script {

	int alchs;
	double VER = 1.1;
	boolean canStart;
	Color FIRE_STAFF = new Color(185, 39, 27);
	Color NATURE_RUNE = new Color(18, 126, 21);
	Color ALCH = new Color(107, 199, 31);

	@Override
	public int loop() {
		if (!Tabs.getCurrentTab().equals("Magic")) {
			Tabs.setTab("Magic");
			sleep(500);
		}
		if (Tabs.getCurrentTab().equals("Magic")) {
			Point p;
			if ((p = ColorUtil.findColor(ALCH, new Point(1, 1), new Point(700,
					500))) != null) {
				Mouse.clickMouse(p.x, p.y, 1, 3, true);
				sleep(50);
				Mouse.clickMouse(p.x, p.y, 6, 6, true);
				alchs++;
			} else {
				// stopScript();
				println("Couldnt find nature runes, looking again...");
			}
		}
		return 1700;

	}


	public boolean onStart() {

		return true;
	}

	@Override
	public void onFinish() {
		println("We managed to alch " + alchs + " items.");
	}
	Color DebugColor = new Color(20,120, 250);

	public Graphics doPaint(Graphics g) {
		g.setColor(new Color(10, 30, 120, 180));
		g.fill3DRect(560, 230, 100, 50, true);
		g.setColor(Color.white);
		g.drawString("ActiveAlcher V" + VER, 565, 240);
		g.drawString("Alchs: " + alchs, 570, 255);
		g.drawString("Exp: " + alchs * 65, 570, 270);
		Point a = Mouse.getLocation();
		if (a != null) {
			int mouse_x = a.x;
			int mouse_y = a.y;
			g.setColor(DebugColor);
			g.drawLine(0, 0, mouse_x-5, mouse_y);
			g.drawLine(0, 503, mouse_x+5, mouse_y);
			g.drawLine(756, 0, mouse_x-5, mouse_y);
			g.drawLine(756, 503, mouse_x+5, mouse_y);
		}
		return g;
	}
}
